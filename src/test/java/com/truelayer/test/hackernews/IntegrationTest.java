package com.truelayer.test.hackernews;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.truelayer.test.hackernews.service.HackerNewsServiceException;
import com.truelayer.test.hackernews.service.scrapper.HackerNewsWebScrapperService;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;

import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

public class IntegrationTest {

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(8089);

    @Test
    public void sanity() {
        stubFor(get(urlEqualTo("/"))
                .willReturn(aResponse().withBodyFile("hn-top.html")));

        List<News> topNews = new HackerNewsWebScrapperService("http://localhost:8089").getTopNews(23);
        Assert.assertNotNull(topNews);
        Assert.assertEquals(23, topNews.size());

        News top = new News("EU drops rule that would prevent self-driving car data from being copyrighted", "https://boingboing.net/2018/10/10/corporate-kitts.html", "lnguyen" , 74, 24, 1);
        Assert.assertEquals(top, topNews.get(0));

        News last = new News("Clues from a Somalian cavefish about modern mammals' dark past", "https://phys.org/news/2018-10-clues-somalian-cavefish-modern-mammals.html", "dnetesn", 39, 3, 23);
        Assert.assertEquals(last, topNews.get(topNews.size() - 1));
    }

    @Test(expected = HackerNewsServiceException.class)
    public void timeout() {
        stubFor(get(urlEqualTo("/"))
                .willReturn(aResponse().withBodyFile("hn-top.html").withFixedDelay(3500)));
        new HackerNewsWebScrapperService("http://localhost:8089").getTopNews(23);
    }
}

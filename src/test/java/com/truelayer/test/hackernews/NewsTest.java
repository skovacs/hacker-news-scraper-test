package com.truelayer.test.hackernews;

import org.junit.Test;


public class NewsTest {

    @Test
    public void sanity() {
        new News("EU drops rule that would prevent self-driving car data from being copyrighted", "https://boingboing.net/2018/10/10/corporate-kitts.html", "lnguyen" , 74, 24, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void titleValidation() {
        String longTitle = "EU drops rule that would prevent self-driving car data from being copyrighted/EU drops rule that would prevent self-driving car data from being copyrighted/EU drops rule that would prevent self-driving car data from being copyrighted/EU drops rule that would prevent self-driving car data from being copyrighted/EU drops rule that would prevent self-driving car data from being copyrighted";
        new News(longTitle, "https://boingboing.net/2018/10/10/corporate-kitts.html", "lnguyen" , 74, 24, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void authorValidate() {
        String longAuthor = "EU drops rule that would prevent self-driving car data from being copyrighted/EU drops rule that would prevent self-driving car data from being copyrighted/EU drops rule that would prevent self-driving car data from being copyrighted/EU drops rule that would prevent self-driving car data from being copyrighted/EU drops rule that would prevent self-driving car data from being copyrighted";
        new News("Hacker News", "https://boingboing.net/2018/10/10/corporate-kitts.html", longAuthor , 74, 24, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void uriValidation() {
        String invalidURI = "abc://google.com";
        new News("EU drops rule that would prevent self-driving car data from being copyrighted", invalidURI, "lnguyen" , 74, 24, 1);
    }

}
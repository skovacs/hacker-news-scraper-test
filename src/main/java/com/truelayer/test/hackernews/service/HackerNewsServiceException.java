package com.truelayer.test.hackernews.service;

public class HackerNewsServiceException extends RuntimeException {

    public HackerNewsServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}

package com.truelayer.test.hackernews.service.scrapper;

import com.truelayer.test.hackernews.Util;
import com.truelayer.test.hackernews.service.HackerNewsService;
import com.truelayer.test.hackernews.News;
import com.truelayer.test.hackernews.service.HackerNewsServiceException;
import lombok.extern.java.Log;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

@Log
public class HackerNewsWebScrapperService implements HackerNewsService {

    private static final String MAIN_PART_SELECTOR = ".athing";
    private static final String SUBTEXT_SELECTOR = ".subtext";

    private static final int TIMEOUT = 3000;
    private static final String HN_POST_PREFIX = "item?id=";
    private static final String DEFAULT_AUTHOR = "Anonymous";
    private static final String NO_BREAK_WHITESPACE = "\\u00a0";

    private final String hackerNewsUrl;

    public HackerNewsWebScrapperService(String hackerNewsUrl) {
        this.hackerNewsUrl = hackerNewsUrl;
    }

    public List<News> getTopNews(int numberOfPosts) {
        int pageNumber = 1;
        List<News> news = new ArrayList<>();
        // Hackernews home page never returns more than 30 posts,
        // so we iterate until we reach numberOfPosts
        while (news.size() < numberOfPosts) {
            news.addAll(loadPage(pageNumber));
            pageNumber++;
        }
        if (news.size() > numberOfPosts) {
            return news.subList(0, numberOfPosts);
        }
        return news;
    }

    private  List<News> loadPage(int pageNumber) {
        Document doc;
        String url = getUrlForPage(pageNumber);
        try {
            doc = Jsoup.connect(url).timeout(TIMEOUT).get();
        }  catch (SocketTimeoutException e) {
            log.severe(e.getMessage());
            throw new HackerNewsServiceException(String.format("Failed to retrieve top news.%nMake sure that you have an internet connection and that %s is accessible", url), e);
        } catch (IOException e) {
            log.severe(e.getMessage());
            throw new HackerNewsServiceException(String.format("Failed to retrieve top news from %s. Error: %s", url, e.getMessage()), e);
        }
        Elements listSelector = doc.select(".itemList > tbody");

        /* One news is composed of two <tr> elements */
        Elements mainPart = listSelector.select(MAIN_PART_SELECTOR);
        Elements additionalPart = listSelector.select(SUBTEXT_SELECTOR);

        return Util.zip(mainPart, additionalPart, this::parseNews);
    }

    private News parseNews(Element a, Element b) {
        Elements rankSelector = a.select(".rank");
        Elements titleSelector = a.select(".title .storylink");
        Elements linkSelector = a.select(".storylink");
        String rank = rankSelector.text();
        String title = titleSelector.text();
        String uri = getURI(linkSelector);

        Elements scoreSelector = b.select(".score");
        String points = scoreSelector.text();

        String author = getAuthor(b);
        String comments = getComments(b);
        return new News(title, uri, author, getIntegerFromRawString(points, "points"), getIntegerFromRawString(comments, "comments"), getIntegerFromRawString(rank, "."));
    }

    private String getComments(Element b) {
        Elements commentsSelector = b.select("a");
        //Comments are in the last link element
        return commentsSelector.get(commentsSelector.size() - 1).text();
    }

    private String getURI(Elements linkSelector) {
        String uri = linkSelector.attr("href");
        if (uri.startsWith(HN_POST_PREFIX)) {
            uri = hackerNewsUrl.concat(uri);
        }
        return uri;
    }

    private String getAuthor(Element b) {
        Elements authorSelector = b.select(".hnuser");
        String author = authorSelector.text();
        // When there is no author (Ex: https://news.ycombinator.com/item?id=18198068)
        if (author.isEmpty()) {
            author = DEFAULT_AUTHOR;
        }
        return author;
    }

    private String getUrlForPage(int pageNumber) {
        return pageNumber > 1 ? hackerNewsUrl + "?p=" + pageNumber : hackerNewsUrl;
    }

    private int getIntegerFromRawString(String comments, String suffix) {
       if (comments == null || !comments.endsWith(suffix)) {
           return 0;
       }
       String number = comments.substring(0, comments.indexOf(suffix));
       try {
           return Integer.parseInt(number.replaceAll(NO_BREAK_WHITESPACE,"").trim());
       } catch (NumberFormatException e) {
           log.warning(e.getMessage());
           return 0;
       }
    }
}

package com.truelayer.test.hackernews.service;

import com.truelayer.test.hackernews.News;

import java.util.List;

public interface HackerNewsService {
    List<News> getTopNews(int numberOfPosts);
}

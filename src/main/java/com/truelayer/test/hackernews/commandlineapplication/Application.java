package com.truelayer.test.hackernews.commandlineapplication;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.truelayer.test.hackernews.News;
import com.truelayer.test.hackernews.service.HackerNewsService;
import com.truelayer.test.hackernews.service.scrapper.HackerNewsWebScrapperService;
import lombok.extern.java.Log;

import java.util.List;

@Log
public class Application {

    private static final String HACKER_NEWS_URL = "https://news.ycombinator.com/news";

    public static void main(String[] args) {
        int numberOfPosts;
        try {
            numberOfPosts = ArgumentParser.getNumberOfPostsFromArguments(args);
        } catch (IllegalArgumentException e) {
            printUsage();
            return;
        }

        try {
            HackerNewsService service = new HackerNewsWebScrapperService(HACKER_NEWS_URL);
            List<News> topNews = service.getTopNews(numberOfPosts);
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            System.out.println(gson.toJson(topNews));
        } catch (Exception e) {
            log.severe(e.getMessage());
        }
    }

    private static void printUsage() {
        System.out.println("Usage:\n hackernews --posts n\n");
        System.out.println("--posts how many posts to print. A positive integer <= 100.\n");
    }
}

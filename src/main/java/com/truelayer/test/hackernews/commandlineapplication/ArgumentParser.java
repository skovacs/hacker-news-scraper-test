package com.truelayer.test.hackernews.commandlineapplication;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ArgumentParser {

    private static final String POSTS_PARAMETER = "--posts";

    public static int getNumberOfPostsFromArguments(@NonNull String[] arguments) {
        if (arguments.length != 2 || !POSTS_PARAMETER.equals(arguments[0])) {
            throw new IllegalArgumentException();
        }
        int numberOfPosts;
        try {
            numberOfPosts  = Integer.parseInt(arguments[1]);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException();
        }
        if (numberOfPosts <= 0 || numberOfPosts > 100) {
            throw new IllegalArgumentException();
        }
        return numberOfPosts;
    }
}

package com.truelayer.test.hackernews;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Util {

    public static <T, V> List<V> zip(List<T> a, List<T> b, BiFunction<T, T, V> zipFunction) {
        int size = Integer.min(a.size(), b.size());
        return IntStream.range(0, size)
                .mapToObj(i -> zipFunction.apply(a.get(i), b.get(i)))
                .collect(Collectors.toList());
    }

    public static void checkArgument(boolean condition, @NonNull String message) {
        if (!condition) {
            throw new IllegalArgumentException(message);
        }
    }
}

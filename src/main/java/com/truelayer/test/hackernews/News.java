package com.truelayer.test.hackernews;

import lombok.Data;
import lombok.NonNull;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

import static com.truelayer.test.hackernews.Util.checkArgument;


@Data
public class News {
    public static final String STRING_VALIDATION_MESSAGE = "%s (%s) must be a non-empty string no longer than 256 characters";

    private String title;
    private String uri;
    private String author;
    private int points;
    private int comments;
    private int rank;

    public News(@NonNull String title, @NonNull String uri, @NonNull String author, int points, int comments, int rank) {
        validateURI(uri);
        checkArgument(isValid(title), String.format(STRING_VALIDATION_MESSAGE, title, "title"));
        checkArgument(isValid(author), String.format(STRING_VALIDATION_MESSAGE, author, "author"));
        this.title = title;
        this.uri = uri;
        this.author = author;
        this.points = points;
        this.comments = comments;
        this.rank = rank;
    }

    private boolean isValid(@NonNull String text) {
        return !text.isEmpty() && text.length() <= 256;
    }

    private void validateURI(String uri) {
        try {
            new URL(uri).toURI();
        } catch (MalformedURLException | URISyntaxException e) {
            throw new IllegalArgumentException(String.format("Invalid uri: %s", uri), e);
        }
    }
}

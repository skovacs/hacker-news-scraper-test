# TrueLayer: Hacker News Scraper Test

A simple Hacker News Web Scraper

### Prerequisites


```
Java 1.8+
Maven 3.5+
```

### Installing

Linux - Unix OS

```
 ./install.sh
```

Windows
```
mvnw.cmd clean install
```

### Usage
Linux / Unix only:
```
./hackernews --posts n
```

All plateforms:
```
java -jar target/hackernews.jar --posts n
```



## Built With
* [Maven](https://maven.apache.org/) - Dependency Management
* [Lombok](https://projectlombok.org/) - Reduce boilerplate code
* [JSoup](https://jsoup.org/)- Http Client and DOM(Html) manipulation
* [Gson](https://github.com/google/gson) - Json Serialization
* [JUnit](https://github.com/junit-team/junit4) - Testing library / Assertion
* [Wiremock](http://wiremock.org/) - Http Mocking


## Authors

**Sebastien Kovacs** (https://github.com/kovacss)